var express = require('express');
var router = express.Router();

const btoa = require('btoa');
const axios = require('axios');

// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

router.get('/', function(req, res, next) {
});

router.get('/new', function(req, res, next) {
  res.render('requests/add', {
    title: 'Request - Add',
    type: req.query.type
  });
});

router.post('/', function(req, res, next) {
  const username = "pademo07";
  const password = "oRacle!2019?";
  const header = "Basic " + btoa(username + ":" + password);
  const base_url = "https://036DEB55A28B4E558D643949487CF145.blockchain.ocp.oraclecloud.com:443/restproxy1/bcsgw/rest/v1/transaction/invocation";

  let id = "IN5"+Math.floor((Math.random() * 1000) + 1);

  let data = {
    "channel": "vita",
    "chaincode": "medsupply1",
    "chaincodeVer": "v0",
    "method": "initBloodRecord",
    "args": [id, "22.5", "44.5", "10.5", "20,5", "Abu Hail Metro", "0", "Blood group 0",  "5", "22.5", "44.5", "1"]
  }

  axios
  .post(`${base_url}`, data, {
      headers: {
          'authorization': header,
          'content-type': 'application/json',
          'accept': 'application/json'
      }
  })
  .then(response => {
    // console.log(data);
    console.log(response);
  })
  .catch(error => {
    console.log(error);
  })

  res.redirect('/');
});

router.get('/:id/scancode', function(req, res, next) {
  let requestId = req.params.id;
  res.render('requests/scancode', {
    title: 'Scan Code',
    requestId: requestId
  });
});

router.get('/:id', function(req, res, next) {
  // Store the request ID
  let requestId = req.params.id;

  var ref = firebase.database().ref('/journey/'+requestId);

  ref.on("value", function(snapshot) {
    let request = snapshot.val();
    res.render('requests/read', {
      title: 'Request - Lol',
      requestId: requestId,
      request: request,
    });
  }, function (error) {
    console.log("Error: " + error.code);
  });
});

router.post('/scancode', function(req, res, next) {
  res.render('requests/scancode');
});

module.exports = router;