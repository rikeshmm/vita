var express = require('express');
var router = express.Router();

// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require("firebase/firestore");
require("firebase/database");

var firebaseConfig = {
  apiKey: "AIzaSyCKQEwiIRISuj1VCJXtWK_A0o0RhF4N-vA",
  authDomain: "infive-d922e.firebaseapp.com",
  databaseURL: "https://infive-d922e.firebaseio.com",
  projectId: "infive-d922e",
  storageBucket: "",
  messagingSenderId: "1000008834199",
  appId: "1:1000008834199:web:f8a1b134197ad4776e1dd2"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('dashboard', {
    title: 'Vita - Dashboard',
  });
});

module.exports = router;