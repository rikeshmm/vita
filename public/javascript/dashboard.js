
// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyCKQEwiIRISuj1VCJXtWK_A0o0RhF4N-vA",
  authDomain: "infive-d922e.firebaseapp.com",
  databaseURL: "https://infive-d922e.firebaseio.com",
  projectId: "infive-d922e",
  storageBucket: "",
  messagingSenderId: "1000008834199",
  appId: "1:1000008834199:web:f8a1b134197ad4776e1dd2"
};
//Initialize Firebase
firebase.initializeApp(firebaseConfig);
const rdb = firebase.database();
let fetchedRecords;
let fetcheddB = rdb.ref().child('journey');
fetcheddB.on('value', function (snapshot) {
  document.querySelector(".pending-requests").innerHTML = ""
  let journeyObj = snapshot.val();
  let index = 0;
  snapshot.forEach(function (doc) {
    // fetchedRecords.push(doc.val())
    console.log(doc.val())
    let key = Object.keys(snapshot.val())[index++]
    rowInsert(doc.val(), key);
  })

})
function rowInsert(obj, key) {
  let requests = document.querySelector(".pending-requests")
  let row = ``;
  row += `<div class="pending-request rounded mb-3">
    <div class="row p-3">
      <div class="col-9">
        <h3 class="m-0">${obj.end_name}</h3>
        <p class="m-0">${obj.end_location}</p>
        <p class="m-0">ReqID: ${key}</p>
      </div>
`
  if (obj.status == -1) {
    row += `      <div onclick="toastr.danger('Kindly contact your supplier for further details','ITEM FAILED TO REACH DESTINATION')" class="col-3 text-right fa-3x">
    <a href="javascript:void(0)"><i class="fas fa-info-circle"></i></a>
  </div>
</div><div class="p-1 text-white text-center status-failed">
        <span>Failed</span>
      </div>
    </div>`
  }
  else if (obj.status == 0) {
    row += `      <div onclick="toastr.warning('Your supplier is yet to begin transporting your package','ITEM IS NOT EN ROUTE')" class="col-3 text-right fa-3x">
    <a href="javascript:void(0)"><i class="fas fa-info-circle"></i></a>
  </div>
</div><div class="p-1 text-white text-center status-pending">
      <span>Pending</span>
    </div>
  </div>`
  }
  else if (obj.status == 1) {
    row += `      <div class="col-3 text-right fa-3x">
    <a href="/requests/${key}"><i class="fas fa-info-circle"></i></a>
  </div>
</div><div class="p-1 text-white text-center status-in-transit">
      <span>In Transit</span>
    </div>
  </div>`
  }
  else if (obj.status == 2) {
    row += `<div class="col-3 text-right fa-3x">
    <a href="/requests/${key}"><i class="fas fa-info-circle"></i></a>
  </div>
</div><div class="p-1 text-white text-center status-completed">
      <span>Completed</span>
    </div>
  </div>`
  }
  requests.innerHTML += row;
}