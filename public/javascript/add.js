var firebaseConfig = {
    apiKey: "AIzaSyCKQEwiIRISuj1VCJXtWK_A0o0RhF4N-vA",
    authDomain: "infive-d922e.firebaseapp.com",
    databaseURL: "https://infive-d922e.firebaseio.com",
    projectId: "infive-d922e",
    storageBucket: "",
    messagingSenderId: "1000008834199",
    appId: "1:1000008834199:web:f8a1b134197ad4776e1dd2"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const rdb = firebase.database();
let fetchedRecords;
let fetcheddB = rdb.ref().child(`journey/`);
let submitBtn = document.querySelector("#btn-submit")
submitBtn.addEventListener('click', function () {
    fetcheddB.push({
        qr_link:'blank',
        current_lat:
            25.244,
        current_lng:
            55.317715,
        current_temp:
            0,
        end_lat:
            25.244,
        end_lng:
            55.3198,
        end_location:
            "Bur Dubai, Dubai",
        end_name:
            "Life Pharmacy",
        is_temp_safe:
            1,
        name:
            $("#name").val(),
        contents:
            $('#contents').val(),
        quantity:
            $("#quantity").val(),
        max_temp:
            $("#max-temperature").val(),
        max_humidity:
            $("#max-humidity").val(),
        manufacture_date:
            $("#manufacture-date").val(),
        expiration_date:
            $("#expiration-date").val(),
        start_lat:
            25.224703,
        start_lng:
            55.317715,
        start_location:
            "Al Jaddaf, Creek, UAE",
        start_name:
            "Pure Health Supply",
        status:
            1
    })
    location.href = "/"
}
)