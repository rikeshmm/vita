
var map;
var busMarkers = [];
var recordID = location.href.split('/').slice(-1).pop()
// Initialize maps instance
function initMap() {
    var sharjah = { lat: 25.2620, lng: 55.3793 };
    var karama = { lat: 25.2089, lng: 55.3061 };
    var options = {
        zoom: 12,
        center: karama,
        styles:
            [
                {
                    "featureType": "all",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "weight": "2.00"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#9c9c9c"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#7b7b7b"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#46bcec"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#c8d7d4"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#070707"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                }
            ],
        mapTypeId: google.maps.MapTypeId.ROADMAP
        //mapTypeId: google.maps.MapTypeId.SATELLITE
    };
    map = new google.maps.Map(document.getElementById('map-tracking'), options);
    // Add a marker
    var markered = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(40.72, -74)
    });



}
initMap();
// function to generate path on maps given latlng objects forming points within the path, in an array
function polyLine(latLngArray) {
    dronePath = new google.maps.Polyline({
        path: latLngArray,
        geodesic: true,
        strokeColor: '#fff',
        strokeOpacity: 1,
        strokeWeight: 1.5,
        map: map
    });
}

// function to create dock marker based on info object
function docksMark(info) {

    var marker = new google.maps.Marker({ position: info.coord, label: info.label, map: map });
    if (info.icon) {
        marker.setIcon(info.icon);
    }
    if (info.window) {

        // Add a Snazzy Info Window to the marker
        var info = new SnazzyInfoWindow({
            marker: marker,
            placement: 'top',
            offset: {

            },
            content: info.window,
            showCloseButton: true,
            closeOnMapClick: true,
            padding: '10px 17px',
            backgroundColor: 'rgba(0, 0, 0, 0.9)',
            border: false,
            borderRadius: '0px',
            shadow: false,
            fontColor: '#fff',
            fontSize: '15px'
        });
        marker.addListener('click', () => {

            // if (isOpen == true) {
            // info.close();
            //     isOpen = false;
            // }
            // else {
            info.open();
            //     isOpen = true;
            // }
        })
    }


}

function calcRoute(start, end) {
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);
    directionsDisplay.setOptions({ suppressMarkers: true, preserveViewport: true });
    var request = {
        origin: start,
        destination: end,
        travelMode: 'DRIVING'
    };
    directionsService.route(request, function (result, status) {
        if (status == 'OK') {
            directionsDisplay.setDirections(result);
            console.log(result)
        }
        else {
            console.log("No")
        }
    });
}

var autoDriveSteps = new Array();
var speedFactor = 15; // 10x faster animated drive

function setAnimatedRoute(origin, destination, map) {
    // init routing services
    var directionsService = new google.maps.DirectionsService;
    var directionsRenderer = new google.maps.DirectionsRenderer({
        map: map
    });

    //calculate route
    directionsService.route({
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING
    },
        function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                // display the route
                console.log("The response", response.routes[0].legs[0].steps);
                directionsRenderer.setDirections(response);

                // calculate positions for the animation steps
                // the result is an array of LatLng, stored in autoDriveSteps
                autoDriveSteps = new Array();
                var remainingSeconds = 0;
                var leg = response.routes[0].legs[0]; // supporting single route, single legs currently
                leg.steps.forEach(function (step) {
                    var stepSeconds = step.duration.value;
                    var nextStopSeconds = speedFactor - remainingSeconds;
                    while (nextStopSeconds <= stepSeconds) {
                        var nextStopLatLng = getPointBetween(step.start_location, step.end_location, nextStopSeconds / stepSeconds);
                        autoDriveSteps.push(nextStopLatLng);
                        nextStopSeconds += speedFactor;
                    }
                    remainingSeconds = stepSeconds + speedFactor - nextStopSeconds;
                });
                if (remainingSeconds > 0) {
                    autoDriveSteps.push(leg.end_location);
                }
            }
            else if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                wait = true;
                setTimeout("wait = true", 2000);
                //alert("OQL: " + status);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
}

// helper method to calculate a point between A and B at some ratio
function getPointBetween(a, b, ratio) {
    return new google.maps.LatLng(a.lat() + (b.lat() - a.lat()) * ratio, a.lng() + (b.lng() - a.lng()) * ratio);
}

// start the route simulation   
function startRouteAnimation(marker) {
    var autoDriveTimer = setInterval(function () {
        // stop the timer if the route is finished
        if (autoDriveSteps.length === 0) {
            clearInterval(autoDriveTimer);
            document.getElementById('scan-qr-code').style.display='block';
            toastr.info("Kindly scan the RFID Tag of your package","Package has arrived")
        } else {
            // move marker to the next position (always the first in the array)
            marker.setPosition(autoDriveSteps[0]);
            // remove the processed position
            autoDriveSteps.shift();
        }
    },
        500);
}

// setAnimatedRoute("Karama","Rashidiya");
// var agentMarker = new google.maps.Marker({ map: map,icon:"../assets/images/schoolbus.png" });
// startRouteAnimation(agentMarker);
// busMarkers.push(agentMarker);
// Your web app's Firebase configuration


var firebaseConfig = {
    apiKey: "AIzaSyCKQEwiIRISuj1VCJXtWK_A0o0RhF4N-vA",
    authDomain: "infive-d922e.firebaseapp.com",
    databaseURL: "https://infive-d922e.firebaseio.com",
    projectId: "infive-d922e",
    storageBucket: "",
    messagingSenderId: "1000008834199",
    appId: "1:1000008834199:web:f8a1b134197ad4776e1dd2"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const rdb = firebase.database();
let fetchedRecords;
let fetcheddB = rdb.ref().child(`journey/${recordID}`);
fetcheddB.on('value', function (snapshot) {
    let journeyObj = snapshot.val();
    if(journeyObj.status==1||journeyObj.status==2){
    displayRequests(journeyObj);
    }



})

function displayRequests(obj) {
   
    let coordArray = [{ lat: obj.start_lat, lng: obj.start_lng }, { lat: obj.end_lat, lng: obj.end_lng }]

    // use directions api to render route
    calcRoute(coordArray[0], coordArray[1]);
    // set start and end markers
    for (let [index, coord] of coordArray.entries()) {
        docksMark({
            label: {
                text: index.toString(),
                // fontWeight: 'bold',
                fontSize: '14px',
                // fontFamily: '"Courier New", Courier,Monospace',
                color: 'white'
            },
            coord: coord,
            icon: {
                path: 'M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z',
                strokeColor: '#df643b',
                fillColor: '#df643b',
                fillOpacity: 1,
                strokeWeight: 4,
                scale: 0.5,
                labelOrigin: new google.maps.Point(0, -25)
            },
            window: `<h5 class="text-uppercase">Place Info</h5><table class="table table-condensed table-bordered">
        <thead>
        <tr>
            <th class="small text-muted text-uppercase"><strong>KEY</strong>
            </th>
            <th class="small text-muted text-uppercase text-right"><strong>VALUE</strong>
            </th>
    
        </tr>
    </thead>
    <tbody>
    <tr class="small v-a-m">
    <td>NAME</td>
    <td class="text-right v-a-m text-white">Place</td>
    </tr>
    <tr class="small v-a-m">
    <td>LAT</td>
    <td class="text-right v-a-m text-white">25</td>
    </tr>
    <tr class="small v-a-m">
    <td>LNG</td>
    <td class="text-right v-a-m text-white">55</td>
    </tr></tbody></table>`


        });
    }
    if(obj.status==1){
        toastr.info("Get ready to receive your package","PACKAGE EN ROUTE TO DESTINATION")
    setTimeout(() => {
        setAnimatedRoute({ lat: obj.start_lat, lng: obj.start_lng }, { lat: obj.end_lat, lng: obj.end_lng });
        var agentMarker3 = new google.maps.Marker({ map: map, icon: "/images/ambulance.png" });
        startRouteAnimation(agentMarker3);
        // busMarkers.push(agentMarker3);
    }, 2000)
    }
    else{
        toastr.success("Thank you for trusting VITA!","PACKAGE SUCCESSFULLY DELIVERED")
    }
}